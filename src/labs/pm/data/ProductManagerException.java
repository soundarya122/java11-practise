package labs.pm.data;

public class ProductManagerException extends Exception {

	public ProductManagerException() {
	}

	public ProductManagerException(String message, Throwable cause) {
		super(message, cause);
	}

	public ProductManagerException(String message) {
		super(message);
	}
	
}
