/*
 * Copyright (C) 2021 soundarya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package labs.pm.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Objects;

/**
 * {@code Product} class represents properties and behaviour of product objects
 * in the Product Management System. <br>
 * Each product has an id, name, and price <br>
 * Each product can have a discount {@link DISCOUNT_RATE discount rate}
 * 
 * @version 4.0
 * @author soundarya
 */
public abstract class Product implements Rateable<Product>, Serializable {

	/**
	 * A constant that defines a {@link java.math.BigDecimal BigDecimal} value of
	 * the discount <br>
	 * Discount rate is 10%
	 */

	public static final BigDecimal DISCOUNT_RATE = BigDecimal.valueOf(0.1);
	private int id;
	private String name;
	private BigDecimal price;
	private Rating rating;
	private LocalDate bestBefore = LocalDate.now();

	Product() {
		this(0, "No name", BigDecimal.ZERO);
	}

	Product(int id, String name, BigDecimal price, Rating rating) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.rating = rating;
	}

	Product(int id, String name, BigDecimal price) {
		this(id, name, price, Rating.NOT_RATED);
	}

	public int getId() {
		return id;
	}

//    public void setId(final int id) {
//        this.id = id;
//    }

	public String getName() {
		return name;
	}

//    public void setName(final String name) {
//        this.name = name;
//    }

	public BigDecimal getPrice() {
		return price;
	}

//    public void setPrice(final BigDecimal price) {
////        price = 2;
//        this.price = price;
//    }

	public Rating getRating() {
		return rating;
	}

//    public void setRating(Rating rating) {
//        this.rating = rating;
//    }

	@Override
	public String toString() {
		return id + ", " + name + ", " + price + ", " + rating.getStars()+", "+ getDiscount();
	}

	/**
	 * Calculates discount based on a product price and {@link DISCOUNT_RATE
	 * discount rate}
	 * 
	 * @return a {@link java.math.BigDecimal BigDecimal} value of the discount
	 */
	public BigDecimal getDiscount() {
		return price.multiply(DISCOUNT_RATE).setScale(2, RoundingMode.HALF_DOWN);
	}

//	public abstract Product applyRating(Rating rating);

	public LocalDate getBestBefore() {
        return bestBefore;
    }
	public void setBestBefore(LocalDate date) {
		bestBefore = date;
	}
	
	@Override
	public int hashCode() {
		int hash = 5;
		hash = 23 * hash + this.id;
		return hash;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
//    	if(this.getClass() != obj.getClass())
//    		return false;
		if (obj instanceof Product) {
			final Product other = (Product) obj;
			if (this.id != other.id)
				return false;
			if (!Objects.equals(other.name, this.name))
				return false;
		}
		return true;
	}

}
